SOURCE_DOCS := $(wildcard *.md)
EXPORTED_DOCS := $(SOURCE_DOCS:.md=.html)

PANDOC_HTML_OPTIONS = --standalone \
                      --table-of-contents --toc-depth=1 \
                      --number-sections \
					  --metadata date="`env TZ=America/New_York date +%m/%d/%Y`" \
                      --css pandoc.css \
                      -V lang=en \
                      -f markdown+smart -t html5

%.html: %.md
	pandoc $(PANDOC_HTML_OPTIONS) -o $@ $<

all: $(EXPORTED_DOCS)

clean:
	rm -f $(EXPORTED_DOCS)
