% Annotated Rules to Inside Pitch

# Basic Procedure

Inside Pitch uses two six-sided dice (2d6) of different colors (traditionally red and white) and one twenty-sided die (1d20, traditionally blue).

An at bat begins by rolling 2d6 against the pitcher's card. Dice are read column first (red die), then row (white die). The 1d20 (blue), which can be rolled at the same time, is used for ratings checks either on the pitcher's card or the batter's card depending on the result.

If the rolled result from the pitcher's card is inconculsive, an additional roll of the 2d6 is necessary. Depending on the type of result from the pitcher's card, this roll is read from either the batter's card or the ballpark card in the same fashion, column first then row.

# Pitcher Card

Pitchers who have faced at least 40 batters from either side of the plate during the carded season will have split results. Read to the left of the `/` when the batter is batting left-handed and read to the right when the batter is batting right-handed. Switch hitters are assumed to be batting from the opposite side from which the pitcher is throwing (batting right-handed against a left-handed pitcher). Results such as `K/` or `/W+` have a blank on the right and left respectively.

## Blank
No result. Roll 2d6 against the batter's card.

## `S1`
Single past the pitcher.

## `(S1)`
If the pitcher is tired, this is a single past the pitcher, otherwise it is a blank.

## `**`
On the pitcher's card, this is an automatic out. Roll 1d6 and read the type of out from the `**` line on the pitcher's card.

>Note that results from the batter's card or ballpark card that reference the pitcher's `**` line are not automatic outs and are subject to any pending range or error checks.

## `WLD`
Possible wild pitch or passed ball. If no runners are on base, disregard as a foul ball and roll again on the pitcher's card.

When runners are on base, roll 1d20 against the pitcher's `WP` and `PB?` ranges. If the roll is in the `WP` range, it's a wild pitch and all runners advance one base.

If the roll is in the `PB?` range, roll an additional 1d6 against the catcher's passed ball rating (the number after the `/` in his defensive ratings). If the roll is less than or equal to the rating, the catcher blocks the ball. If the roll is greater than the passed ball rating, it is a passed ball.

If neither a wild pitch nor a passed ball is rolled, the result is a foul ball.

After resolving these checks, continue the at bat by rolling again (2d6) on the pitcher's card. [3665]

## `(??)`
Roll 1d20 against the `??` split rating on the pitcher's card, either the `LHB` rating against left handed batters or the `RHB` rating for right handers. If the roll is in range, it is either a `**` or `S1` result as listed on the card. If the 1d20 roll is outside the range or there is no range, it is a blank with no result. Proceed to roll 2d6 against the batter's card.

>This result applies whether or not the pitcher is tired. The parentheses are a reminder of the [exhausted pitcher rule](#exhausted-optional) discussed later.

## `@`
Roll 2d6 against the ballpark card to resolve the play.

## `E?`
Possible error. Roll 2d6 against the batter's card to resolve the play. On all results, check for the error by rolling 1d20 against the fielder's error rating. Consult the [`E?` Error Chart](#general-errors).

## `ET?`
Possible throwing error. Roll 2d6 against the batter's card to resolve the play. Check for an error only on plays involving throws. Do this by rolling 1d20 against the fielder's error rating. Check for a throwing error on all hits where runner advancement requires a throw from an outfielder or a hit to an infielder that is considered knocked down but possibly thrown away. Consult the [`ET?` Error Chart](#throwing-errors).

## `EG?`
Possible error on ground balls (`G`) only. Roll 2d6 against the batter's card to resolve the play. On ground ball results, check for an error by rolling 1d20 against the fielder's error rating. Do not check for errors on infield hits, flyouts, popups, or line drives. Consult the [`EG?` Error Chart](#groundball-errors).

## `HBP`
Possible hit-by-pitch. Roll 1d20 against the sum of the batter's and pitcher's `HBP` ratings. If the sum is less than one, then no HBP can occur. If the roll is less than or equal to the combined rating, the batter is hit by the pitch and is awarded first base. If the roll is greater than the combined rating, proceed to roll 2d6 against the batter's card.

## `K` and `K+`
Possible strikeout. Roll 1d20 against the sum of the batter's `K` rating and any ballpark `K` adjustment. If `K+`, add ten to the sum. If the roll is less than or equal to the adjusted rating, the batter strikes out.  Otherwise, proceed to roll 2d6 against the batter's card.

## `W` and `W+`
Possible walk. Roll 1d20 against the sum of the batter's `W` rating and any ballpark `W` adjustment. If `W+`, add ten to the sum. If the roll is less than or equal to the adjusted rating, the batter walks. Otherwise, proceed to roll 2d6 against the batter's card.

## `K(W)`
When the pitcher is not tired, proceed with a `K` possible strikeout result. If the pitcher is tired, proceed with a `W` possible walk result.

## `HR`
Possible homerun. Roll 1d20 against the batter's `HR` rating. In rare instances, the ballpark card has an adjustment for `HR`. If so, apply that to the batter's rating. If the roll is less than or equal to the adjusted rating, it's a homerun, otherwise it is a blank with no result (proceed to roll 2d6 against the batter's card).

## `HR?`
Possible homerun. Roll 1d20 against the `HR?` ranges on the pitcher's card using the batter's handedness (`LHB` or `RHB`). If the roll is within the range, treat the result as an `HR` result (follow directions above). If the roll is outside the range, it is a blank with no result (proceed to roll 2d6 against the batter's card). 

>The ballpark card `HR` rating is not used to adjust this range. [5043] 

## `RP`
[Range play](#range-plays). Roll 2d6 against the *batter's card* to resolve the play. Next, roll 1d6 against the fielder's range rating. If the roll is less than or equal to the fielder's range rating, the fielder makes the play.

## `RP@`
[Range play](#range-plays). Roll 2d6 against the *ballpark card* to resolve the play. Next, roll 1d6 against the fielder's range rating. If the roll is less than or equal to the fielder's range rating, the fielder makes the play.

## `Special K LHB` or `Special K RHB`
Some pitchers need additional chances for strikeouts beyond those that the 36 cells on a pitcher's card allow. These pitchers have `Special K` ratings. For pitchers with these additional ratings, all results from the batter's card or ballpark card *other than hits* become strikeouts if a 1d20 roll is within the `Special K` range.

However, if either a range or error check has been activated by the pitcher's card, both checks take precedence and no `Special K` check is made, regardless of the final result. Any rare play also cancels out the `Special K` check.

>Often the pitchers with `Special K` ratings are found in eras of high strikeout rates. But the pitchers that require extra strikeout chances are not always those with the highest strikeout rates. Excessive walks or, more likely, an extremely low rate of balls in play might necessitate `Special K` ratings. [5339.31]

>Results directly from the pitcher's card of `**` are not subject to special K checks.

>Results from the batter's card or ballpark card that reference the pitcher's card `**` line (`**1` through `**6`) are included in checks for special K's. Treat these references as if the result on the pitcher's line is replacing the `**#` entry on the batter's card.

>Example: Pitcher's card is a `K`, batter's `K` rating is a 10 (ballpark `K=-2`) and the 1d20 is a 9. The batter narrowly avoids the strikeout. Rolling 2d6 on his card results in a `G4`. The pitcher's `Special K` rating is 1-10 (not altered by the ballpark `K` rating). Roll a 1d20 to see if the groundout is swapped for a strikeout (rolls of 1-10). [4006]

>Example: Pitcher's card is `@`, ballpark card is `?9`, and a 1d20 check against the `H?` range yields an `HR` result. Roll another 1d20 against the batter's `HR` rating. If the roll is less than or equal to that rating, it's a homerun. If the roll is greater than that rating, the normal result is an `F9` and because it's not a hit, the `Special K` rating is used. If another 1d20 roll is inside the `Special K` rating, the flyout is swapped for a strikeout. [4706]

>Example: Pitcher's card is an `RP`, batter's card is a `G6`. Since a range check is necessary, there is no `Special K` check, regardless of result. Simply roll the 1d6 against the shortstop's range rating and resolve as a normal range play. [5339]

>Example: Pitcher's card is an `E?`, batter's card is a `F8`. Since an error check is necessary, any `Special K` rating a pitcher may have is irrelevant. Roll 1d20 against the centerfielder's error rating and resolve as a normal error check play. [5339]

# Batter Card

Batters who have made at least 40 plate appearances from both sides of the plate in the carded season will have `/` split results in the grid as well as split `K`, `W`, and `HR` ratings. Read to the left of the `/` when the pitcher is pitching left-handed and read to the right when the pitcher is pitching right-handed.

When checking `K`, `W`, or `HR` ratings, read the `LHP` line against left-handed pitchers and the `RHP` line against right-handed pitchers. In both cases, remember to apply any adjustments to these ratings from the ballpark card.

## `**1`, `**2`, `**3`, `**4`, `**5`, and `**6`
Read the number against the pitcher's card `**` line to determine the type of out. Apply any range or error checks to the fielder.

## `?7`, `?8`, and `?9`
Possible hit to the outfield (7=LF, 8=CF, 9=RF). Roll 1d20 against the `LH?` or `RH?` splits on the batter's card depending on the handedness of the pitcher. Batters without splits will have a range of `H?` results. Ranges may be given for singles, doubles, triples, and home runs. Rolls outside all the ranges are flyouts. Resolve any range or error checks after first determining the batted ball type. [4031] Home run results are automatic. They are not influenced by range plays, error checks, or the batter's `HR` rating.

>Example: `RH?: S=1-8; D=9-16; T=17-18` where 19-20 is a flyout.

## Hits: `S`, `D`, `T`, `H`
Single, double, triple, and home run, respectively. Singles to infield positions are considered singles through the infield past that player. Any `H` results are automatic homeruns without the need for a 1d20 roll against the batter's `HR` rating.

## Outs: `G`, `P`, `F`, `L`
Groundout, popup, flyout, lineout, and strikeout respectively.

>Example: `S/G6` single past the shortstop against a left-handed pitcher, groundout to shortstop against a right-handed pitcher.

>Example: `F/D9` flyout to the right fielder against a lfet-handed pitcher, double to right field against a right-handed pitcher.

## Rare: `W`, `K`
Walks and strikeouts in the batter's grid are rare and appear only on player's cards with extreme hitting stats, mostly pitchers. These results override any range or error checks. [4697]

# Ballpark Card

Results of `@` and `RP@` on the pitcher's card lead to rolls of 2d6 against the ballpark card, rather than the batter's card.

## Blank
Go to the rare play charts and roll 2d6. There are two charts: one for bases empty situations and the other when runners on base. Use the appropriate chart as the game situation dictates.

>Any range or error check from the pitcher's card is negated by the rare play result. [3323]

## Typical Hits (`S`, `D`, `T`) and Outs (`G`, `P`, `F`, `L`)
Read these results just as you would from the batter's card.

## `**1`, `**2`, `**3`, `**4`, `**5`, and `**6`
Read the number against the pitcher's card `**` line to determine the fielder. Apply any range or error checks to the fielder.

## `?7`, `?8`, and `?9`
Possible hit to the outfield (7=LF, 8=CF, 9=RF). Roll 1d20 against the `H?` splits on the ballpark card. Ranges may be given for singles, doubles, triples, and home runs. Rolls outside all the ranges are flyouts.

`[HR]` results remind that a 1d20 roll against the batter's card is necessary. A failed `HR` check results in a flyout. Range play checks on fielders are made only after the home run check fails.

>Example: Pitcher's card is an `RP@`, ballpark card yields a `?7`. Step one, roll 1d20 to determine batted ball type.

> * If the roll lands in the `H` range for a homerun possibility, roll 1d20 against the batter's `HR` rating. If less than or equal to the rating, it's a home run and no range check takes place. If greater, it's an `F9` that requires a 1d6 range check on the left fielder. If he fails to make the play, assume a double off the wall. [2090]
> * If the original 1d20 roll is within the ranges for a single, double, or triple, roll 1d6 against the left fielder's range rating. If the fielder makes the play, it's an `F7`. If not, the type of hit stands.
> * If the original 1d20 roll is outside the hit ranges, it's an `F7`. Roll 1d6 against the left fielder's range rating. If he fails to make the play, roll one final 1d6 to determine single (1-4) or double (5-6). [2864]

## `[HR8]`, `[HRo]`, `[HRp]`
Possible home runs to center field, opposite field, and pull-side field, respectively. Roll 1d20 against the batter's `HR` rating (adjusted by ballpark `HR` rating if any). Rolls less than or equal to the rating are home runs. If the roll is greater than the rating, play it as a flyout.

Range play checks on fielders are made only after the home run check fails. A right-handed batter's pull-side field is to left field, with the opposite field being right field.

If the fielder fails the range check, the result is a double. If the play is made, runners on 2nd and 3rd advance automatically.

>Example: A right-handed batter with a 13 `HR` rating against left-handers gets an `RP@` result off a left-handed pitcher's card. The ballpark yields an `[HRp]` result and comes with an `HR` ballpark rating of 0. Roll 1d20 against the total `HR` rating of 13, with 1-13 being a homerun to left field. On rolls of 14-20, the range play is triggered. If the left fielder has a range rating of 4, 1d6 rolls of of 1-4 yield an `F7` and rolls of 5-6 are a `D7`.

## `S+7`, `S+8`, `S+9`
Single with the possiblity of an extra base. Roll 1d6 against the combination of the batter's `BR` rating and the outfielder's arm rating. If the roll is less than or equal to this sum, the runner is safe at second with a double. Rolls greater than the sum, but not equal to 6 are singles only. If the roll is greater than the sum and equal to 6, the runner is thrown out at second trying to stretch the single. All other runners advance two bases, regardless of the hitter's outcome.

With two outs and runners going on contact, a runner on second scores before any possible out at second. [4915]

If this result is arrived at by way of an `RP@` result from the pitcher's card, remember to resolve the range play first and see if the outfielder can make the play. If the range check fails, roll 1d6 to see if the batter can take the extra base.

>Example: The result from the pitcher's card is `RP@` and the result from the batter's card is `S+7`. First, roll 1d6 against the range rating of the left fielder to see if he makes the play. For a range rating of 4, rolls of 1-4 would be an `F7` result and rolls of 5-6 would mean the `S+7` prevails. Assuming the batter's `BR` rating is 3 and the outfielder's arm rating is -1, roll 1d6 against the sum of 2. Rolls of 1-2 and the batter legs out a double. Rolls 4-5 and he holds at first. A roll of 6 means he is thrown out at second by the left fielder. [4503, 2054]

>Example: Batter with a `BR` rating of 4 hits an `S+9` to a right fielder with an arm rating of +2. Since the sum is 6, all rolls of the 1d6 put the batter safely on second with a double and no throw from the outfielder. [4775]

# Errors

After an error possibility (`E?`, `EG?`, `ET?`) result from the pitcher's card, the result from the hitter or ballpark card may be subject to an error check. To check for an error, roll 1d20 against the appropriate position player's error rating. If the roll is less than or equal to the rating, an error occurs. If the roll is greater, no error occurs and the play should be completed as usual.

The charts that follow denote the type and number of bases for each type of error. If the table shows a blank for a particular scenario, then no error can occur, so play the result from the batter's card normally.

Hits with errors are just that--a hit with additional advancement due to an error.

Outs with errors are considered an error with the runner reaching base.

## General Errors

: `E?` Error Check Roll

| Position |`G` | `P`  | `L` |`F` |`S` |`D` |`T` |
|:--------:|:--:|:----:|:---:|:--:|:--:|:--:|:--:|
| 1 (P)    | #1 | #2   |     |    | #4 |    |    |
| 2 (C)    | #1 | Foul |     |    | #4 |    |    |
| 3 (1B)   | #1 | #2   |     |    | #4 |    |    |
| 4 (2B)   | #1 | #2   |     |    | #4 |    |    |
| 5 (3B)   | #1 | #2   |     |    | #4 |    |    |
| 6 (SS)   | #1 | #2   |     |    | #4 |    |    |
| 7 (LF)   |    |      |     | #3 | #5 | #5 | #5 |
| 8 (CF)   |    |      |     | #3 | #5 | #5 | #5 |
| 9 (RF)   |    |      |     | #3 | #5 | #5 | #5 |

\#1 (`G`): Infielder boots the ball and the batter is safe at first, all other runners advance one base.

\#2 (`P`): Infielder drops the popup. With less than two outs, the batter is safe at first, and all other runners advance one base. With two outs, the batter advances to second and all others advance two bases. `P2` result is a foul ball, replay the at bat.

>The infield fly rule takes effect on a fair popup result with less than two outs and a force out at third. This means that the batter is out and all other runners hold. Any error check is negated.

\#3 (`F`): This is a rare occurence, so an error only occurs if the 1d20 roll is less than or equal to the error rating AND odd. If the roll is even, no error occurs and the outfielder makes the catch (normal flyout). If odd, the outfielder drops the fly ball, the batter reaches first, and everyone advances one base. Roll 1d6. Working backwards from the lead runner to include the batter, if the roll is less than or equal to the `Adjusted BR` rating of that runner, he advances one base further.

>On a dropped fly ball with 2 outs, all runners advance 2 bases, regardless of `BR` [5120].

\#4 (`S1` to `S6`): Infield single and the infielder throws the ball away to allow the batter to advance to second. All other runners advance two bases. If no error occurs, all runners advance only one base.

\#5 (OF Hit): Single, double, or triple with at least one additional base advance. Roll 1d6 and compare to the `Adjusted BR` ratings of the hitter and any baserunners. If the roll is less than or equal to their rating, that runner or hitter advances an additional base for a total of two bases due to the error.

>For \#3 and \#5, the base being tried for has no impact on the play: `Adjusted BR` = `BR` + `OF Arm` + 1 (if 2 out) [5342]

>Example: 0 or 1 out, Runner on first (`BR`=3), batter `BR`=2. Pitcher's card is `EG?`, batter's card yields an `S8`. If the centerfielder fails the error check, the batter singles and advances to second on the error. The runner from first advances to third. The `OF Arm` rating of the centerfielder is +1. Roll 1d6. Lead runner on third `Adjusted BR`=3+1=4. Runner scores on rolls of 1-4, everyone holds on rolls of 5-6. Using the same roll, if the lead runner scores, check against `Adjusted BR` of the batter, which is 2+1=3. Rolls of 1-3 and he takes third on the error. With 2 outs, add 1 to each runner's `Adjusted BR`. [5342]

## Throwing Errors

: `ET?` Error Check Roll

| Position |`G` |`P` | `L` | `F`|`S` |`D` |`T` |
|:--------:|:--:|:--:|:---:|:--:|:--:|:--:|:--:|
| 1 (P)    | #6 |    | #7  |    | #4 |    |    |
| 2 (C)    | #6 |    |     |    | #4 |    |    |
| 3 (1B)   | #6 |    | #7  |    | #4 |    |    |
| 4 (2B)   | #6 |    | #7  |    | #4 |    |    |
| 5 (3B)   | #6 |    | #7  |    | #4 |    |    |
| 6 (SS)   | #6 |    | #7  |    | #4 |    |    |
| 7 (LF)   |    |    |     |    | #8 | #8 | #8 |
| 8 (CF)   |    |    |     |    | #8 | #8 | #8 |
| 9 (RF)   |    |    |     |    | #8 | #8 | #8 |

\#6 (`G`): Check for a throwing error on the fielder. If there's a possible double play chance, check also for a throwing error on the pivot man. If either makes an error, it is a throwing error with the potential for advancing another base on bad throw. Follow these steps:

1. Roll 1d6 against the range of the throw reciever; if less than or equal to his range he kept the poor throw from getting by him making it only a 1 base error, otherwise proceed to step 2.
2. Roll 1d6 against all runners' (including batter) `BR` ratings; If the roll is less than or equal to the runner(s)/hitter's `BR` rating, each gets an extra base. If the roll is higher than the rating, no advance. Note that there must be an available base to move up, otherwise no advance.

\#7 (`L`): An error can only occur if runners are on base with less than 2 outs. On a line drive, the ball is caught. It is a one base throwing error trying to double off a runner. If no error occurs, then double off the closest baserunner to position where the ball was hit.

\#8 (LF, CF, RF): These throwing errors of *one additional base* are only applicable if rolling for runner advancement, otherwise no error.

## Groundball Errors

: `EG?` Error Check Roll

| Position |`G` |`P` | `L` | `F`|`S` |`D` |`T` |
|:--------:|:--:|:--:|:---:|:--:|:--:|:--:|:--:|
| 1 (P)    | #1 |    |     |    |    |    |    |
| 2 (C)    | #1 |    |     |    |    |    |    |
| 3 (1B)   | #1 |    |     |    |    |    |    |
| 4 (2B)   | #1 |    |     |    |    |    |    |
| 5 (3B)   | #1 |    |     |    |    |    |    |
| 6 (SS)   | #1 |    |     |    |    |    |    |
| 7 (LF)   |    |    |     |    | #5 | #5 | #5 |
| 8 (CF)   |    |    |     |    | #5 | #5 | #5 |
| 9 (RF)   |    |    |     |    | #5 | #5 | #5 |

\#1 and \#5: Follow the procedures listed above for the [`E?` Error Chart](#general-errors).

# Range Plays

After a range play result (`RP` or `RP@`) from the pitcher's card, all outcomes from the hitter's card or ballpark card are subject to a range check.

Roll 1d6 against the listed position's range rating. If the roll is less than or equal to the rating, the fielder makes the play, otherwise he does not. Out results can become hits and hit results can become outs as a result of the range check.

The following adjustments are to be made depending on the positioning of the infielder making the play:

- Infielder Back: No changes
- Infielder Halfway: `Range - 1` (no adjustments for P or C)
- Infielder In: `Range - 2` (no adjustments for P or C)

: Failed Range Check Chart

|Position| `G` | `P`  | `L` | `F` | `S` | `S+`| `D` | `T` |
|:------:|:---:|:----:|:---:|:---:|:---:|:---:|:---:|:---:|
| 1 (P)  |`S1` | `S1` |`S1` |     |`S1` |     |     |     |
| 2 (C)  |`S2` | Foul |     |     |     |     |     |     |
| 3 (1B) |`#3` | `S3` |`#3` |     |`S3` |     |     |     |
| 4 (2B) |`S4` | `S4` |`S4` |     |`S4` |     |     |     |
| 5 (3B) |`#5` | `S5` |`#5` |     |`S5` |     |     |     |
| 6 (SS) |`S6` | `S6` |`S6` |     |`S6` |     |     |     |
| 7 (LF) |     |      |     |`#7` |`S7` |`S+7`|`D7` |`T7` |
| 8 (CF) |     |      |     |`#8` |`S8` |`S+8`|`D8` |`T8` |
| 9 (RF) |     |      |     |`#9` |`S9` |`S+9`|`D9` |`T9` |

\#: Roll 1d6:

- 1-4 = Single (`S3` or `S5`), follow [normal runner advancement rules](#singles-through-the-infield)
- 5-6 = Double (`D3` or `D5`) with 2 base advancement only [2293, 5598]

: Successful Range Check Chart

| Pos.   | `G` | `P`  | `L` | `F` | `S` |`S+` | `D` | `T` |
|:------:|:---:|:----:|:---:|:---:|:---:|:---:|:---:|:---:|
| 1 (P)  |`G1` |`P1`  |`L1` |     |`G1` |     |     |     |
| 2 (C)  |`G2` | Foul |     |     |     |     |     |     |
| 3 (1B) |`G3` |`P3`  |`L3` |     |`G3` |     |     |     |
| 4 (2B) |`G4` |`P4`  |`L4` |     |`G4` |     |     |     |
| 5 (3B) |`G5` |`P5`  |`L5` |     |`G5` |     |     |     |
| 6 (SS) |`G6` |`P6`  |`L6` |     |`G6` |     |     |     |
| 7 (LF) |     |      |     |`F7` |`F7` |`F7` |`F7` |`F7` |
| 8 (CF) |     |      |     |`F8` |`F8` |`F8` |`F8` |`F8` |
| 9 (RF) |     |      |     |`F9` |`F9` |`F9` |`F9` |`F9` |

>Example: Pitcher's card yields an `RP` result, batter's card is a `D8`. The range check puts this potential double to center field in question. The range rating of the center fielder is a 3. Roll 1d6. Rolls 1-3 and the play is made (successful range check): `F8`.  Rolls 4-6 and the fielder fails the range check, resulting in the original `D8` double to center.

>Example: Pitcher's card yields an `RP` result, batter's card is an `L3`. Will the first baseman make the play? The first baseman's range rating is a 2. Roll 1d6. Rolls 1-2 and the play is made: `L3`. Rolls 3-6 and the out result becomes a hit. Roll an additional 1d6 to determine if it's an `S9` (1-4) or `D9` (5-6).

>Example: Pitcher's card yields an `RP@` result, ballpark card is an `S+9`. This is potentially a single to rightfield with the the chance for an extra base. The range rating of the right fielder is a 2. Roll 1d6. Rolls 1-2 and the play is made: `F9`. Rolls 3-6 and the original result `S+9` stands. Roll another 1d6 to see if the batter can stretch the single into a double. If the right fielder's arm rating is a -1 and the batter's `BR` rating is 4, rolls $\leq$ 3 and it's a double, rolls = 4 or 5 and it's a single, and on a 6, the batter is thrown out at second base.

>Example: With the infield in, the game on the line, and the tying run at third with one out, an `RP` result is followed by a `G6`. Will the shortstop (range of 4) make the play? Because he is playing in, the shortstop's range is reduced from 4 to 2. Roll 1d6. Rolls 3-6 and it's a single past the shortstop and the run scores. On rolls 1-2, the he makes the play and the decision of whether to send the runner home must be made. Two possibilities emerge: (1) The runner holds (shortstop looks the runner back) and throws to first for the `G6` or (2) the runner goes and the shortstop attempts to throw him out. In the latter case, the shortstop's positioning reduces the runner from third's `BR` rating by 2. Roll 1d6. A `BR` 3 runner becomes a 1 and he's out on rolls 2-6, safe only on a 1.

# Runner Advancement on Hits

On hits, the batter and all baserunners automatically advance at least as far as the hit would indicate (one base for a single, two for a double, etc.) What follows are the rules that govern whether any of the runners (batter included) advance further than that.

## Singles through the Infield
The simplest scenarios are singles through the infield (`S1`-`S6`). In these cases, only the `BR` rating of each runner is used to determine advancement. The batter always holds at first. Use the following chart and work backwards from the lead runner. If a runner's `BR` rating is in the range shown, he takes the extra base. When there are two outs and runners are going on contact, increase each runner's `BR` rating by one.

>With 2 outs, add 1 to the `BR` rating.

: Infield Singles Advancement Chart

|       | 1st to 3rd | 2nd to Home |
|:-----:|:----------:|:-----------:|
| `S3`  |    2-6     |     4-6     |
| `S4`  |    3-6     |     2-6     |
| `S1`  |    4-6     |     1-6     |
| `S6`  |    5-6     |     3-6     |
| `S5`  |     6      |     5-6     |

>`S2` results only advance runners one base, with no opportunity for additional advancement. [1436]

>Example: With a runner on second (`BR` = 3) and one out, the batter singles past the first baseman (`S3`). Since his `BR` rating of 3 is below the 4-6 range required to score, the runner from second holds at third. Had that same single come with two outs, his `BR` rating would have increased to 4 and been within the required range to score from second on an `S3`.

>Example: With runners on first (`BR` = 5) and second (`BR` = 2) and one out, the batter singles past the shortstop (`S6`). Working backwards, the runner from second must hold at third with only a one base advance. His `BR` is just under the 3 needed to take the extra base and score. As a result, the runner from first is held up by the lead runner and must stop at second. This is despite the fact that his `BR` of 5 allows for a two base advance. Had there been two outs, both runners would be going on contact and have their ratings increase. The lead runner would then be inside the 3-6 range required to score and the runner from first could advance to third.

## Singles and Doubles to the Outfield
Singles and doubles to the outfield (`S7`, `S8`, `S9`, `D7`, `D8`, `D9`) can be a bit more complicated due to the possiblity of an outfield assist. In addition to taking the extra base, runners behind the play may move up when throws are made to a base.

>Triples (`T7`, `T8`, `T9`) are easy to resolve because all runners score and the batter holds at third.

The "lead runner" is considered to be the most advanced baserunner *on which a play can be made*. All other runners, including the batter, are considered "trailing runners."

>Example: A runner on third when the batter singles is, for the purposes of determining extra bases, not considered to be the "lead runner." This is because he will always score and never draw a throw home. The same goes for runners on second and third when a double is hit. The lead runner in that scenario would have to be on first (the batter is never the lead runner). In most cases, the batter is the only trailing runner.

The first step is to use the following formula to adjust the lead runner's `BR` rating:

`Adjusted BR` = `BR` + Chart + `OF Arm` + (1 if 2 out)

: Hits to the Outfield `BR` Adjustment Chart

|       | 1st to 3rd | 2nd to Home | 1st to Home |
|:-----:|:----------:|:-----------:|:-----------:|
| `S7`  |     -2     |     +1      |             |
| `S8`  |     -1     |     +2      |             |
| `S9`  |     +2     |     +1      |             | 
| `D7`  |            |             |     -1      |
| `D8`  |            |             |     +1      |
| `D9`  |            |             |     -1      |

The `Adjusted BR` rating of the lead runner is used to determine if a throw from the outfielder is possible:

: Is a Throw Possible?

| `Adjusted BR` | Result                                                 |
|:-------------:|:-------------------------------------------------------|
|     < 1       | Lead runner holds, without a throw.                    |
|     1-5       | Possible throw. Roll 2d6 and proceed to the next step. |
|      6+       | Lead runner advances, without a throw.                 |

>On a single with runners on first and second (or bases loaded), if the lead runner from second scores without a throw (`ABR` $\geq$ 6), the runner from first becomes the "lead runner." Repeat the adjustment of this new runner's `BR` rating. The batter is never the "lead runner."

A throw is possible when the `Adjusted BR` is 1-5. When a throw is possible, roll 2d6 and check the first die against the following chart.

>This roll of 2d6 is used for *all* runners. Another roll (1d6) is required only if a rundown ensues.

: Hits to the Outfield Advancement Chart

| Roll | 1st Die                                                                    |
|:----:|:---------------------------------------------------------------------------|
|  1   | Lead runner advances. Use 2nd die for *each* trailing runner with Chart A. |
| 2-5  | If roll is less than or equal to `Adjusted BR`, advance, otherwise hold.   |
|  6   | A runner is very likely thrown out! Use 2nd die with Chart B.              |

>Note: 2-5 result, this check applies to the lead runner and all trailing runners except the batter. Work backwards, checking each runner individually for an advance. [5413] The batter can only advance on rolls of 1 or 6.

: Chart A (1st die roll of 1)

| Roll      | 2nd Die                                                                                       |
|:---------:|:----------------------------------------------------------------------------------------------|
|$\leq$ `BR`| If roll is less than or equal to the trailing runner's `BR` rating, he advances on the throw. |
| > `BR`    | If roll is greater than the trailing runner's `BR` rating, he holds.                          |

>No adjustments to `BR`

: Chart B (1st die roll of 6)

| Roll | 2nd Die                                                                                                                                             |
|:----:|:----------------------------------------------------------------------------------------------------------------------------------------------------|
|  1   | Lead runner advances, ball cutoff, and the first trailing runner is thrown out. If the batter is not the first trailing runner, he holds at first.  |
| 2-5  | Lead runner thrown out; If trailing runner's `BR` rating is greater than or equal to the roll, then he advances on the throw.                       |
|  6   | Lead runner advances, ball cut off, and the first trailing runner is caught in a rundown. Roll 1d6 against rundown chart.                           |

>No adjustments to `BR`

: Rundown Chart

| Roll | 1d6                                                                                                                                         |
|:----:|:--------------------------------------------------------------------------------------------------------------------------------------------|
|  1   | Runner out quickly. No time for others to advance.                                                                                          |
| 2-5  | Runner out. If the roll > `BR` of runner who is tagged out in the rundown, then no advance for others. If roll $\leq$ `BR`, others advance. |
|  6   | Runner safe, gets under tag at advance base.                                                                                                |

>Example: Bases loaded, one out. The runner on 2nd (`BR` = 4), runner on 1st (`BR` = 3), batter (`BR` = 3). `S8` result, center fielder's arm is -1. On the single, everyone advances one base, scoring the runner from 3rd. The lead runner is the runner from 2nd, his `Adjusted BR` is 4 + 2 - 1 = 5. A throw is possible, so roll 2d6.

> * Roll is 3, 6. The 3 is checked against the lead runner's `Adjusted BR` first. His rating beats the roll (5 $\geq$ 3) and he scores. The runner on 2nd's `Adjusted BR` is 3 - 1 - 1 = 1. His rating does not beat the roll of 3, so he holds at 2nd. The batter can go no further than first. The second die of 6 is not needed.
> * Roll is 1, 3. The 1 means that the runner on 2nd scores and the runner on 1st's `BR` rating is compared with the second die roll of 3. His `BR` rating, unadjusted, is 3, which is equal to the roll. He takes the extra base and reaches 3rd. The batter's `BR` rating of 3 means that he takes 2nd on the throw.
> * Roll is 6, 2. The 6 immediately means there's a 97% chance someone is going to be thrown out! Going to Chart B, the 2 means that the lead runner is thrown out at the plate! The runner from first and the batter both have `BR` ratings $\geq$ the roll of 2. They both advance on the throw. The play ends with the batter on 2nd and the runner from 1st at 3rd.

# Groundouts

## No runners on
Simple groundout.

## Runner on 1st, less than 2 outs
Compare 1d6 roll against the `Combined GDP` rating, a sum of the following:

 - Pitcher `GDP`
 - Batter `GDP`
 - Pivot Rating (See table below for which fielder's rating to use given location and handedness of the batter.)
 - +1 (If double play ready: the initial fielder is playing halfway or in and the pivot man is halfway. For this calculation, the pitcher is always considered halfway.)

If the roll is less than or equal to the `Combined GDP`, the double play is successfully turned. Otherwise, no double play.

If no double play, compare that roll to the `BR` rating of the runner on first (reduced by 1 if the fielders involved are double play ready). If the roll is less than or equal to the `BR` rating OR is equal to 6, the runner is safe at second and the only play is at first.

If the roll is greater than the `BR` rating of the runner on first but not equal to 6, the runner is out on the force play.

: Pivot Table

|      | LHB | RHB |
|:----:|:---:|:---:|
| `G1` | SS  | 2B  |
| `G2` | SS  | 2B  |
| `G3` | SS  | SS  |
| `G4` | SS  | SS  |
| `G5` | 2B  | 2B  |
| `G6` | 2B  | 2B  |

>Example: Result is `G6`. Batter's `GDP` = 1, Pitcher's `GDP` = 0, Pivot of 2B is +1, infield is halfway (+1) `Combined GDP` = 3. Roll 1d6. Rolls 1-3 are 643 double play results. Rolls 4-6 are not double plays. Runner on 1B `BR` = 3, which becomes 2 for infield halfway. Rolls 3-5 are 64 force outs at second. Roll of 6 is a 63 ground out, with the runner advancing to second.

>Same Example, Runner on 1B `BR` = 5, which becomes a 4 since the infield is halfway. On rolls of 4 and 6 the runner is safe at second, 63 groundout. Roll of 5 and the runner is forced at second, batter safe at first.

>Example: Result is `G4`. Batter's `GDP` = 4, Pitcher's `GDP` = 0, Pivot of SS is +1, infield is playing halfway (+1). `Combined GDP` = 6. All rolls of the 1d6 are double plays.

>Example: Pitcher's card is a range play (`RP`), batter's card result is `G3`. Resolve the range play first and reduce the first baseman's range rating by 1 if he's playing halfway. If he makes the play, follow the typical double play procedure. [5799]

## Runner on 2nd, less than 2 outs
 - `G2`, `G3`, `G4`: Runner on second advances to third.
 - `G1`, `G6`: Roll 1d6. Runner advances if roll is less than or equal to runner's `BR` rating, otherwise hold.
 - `G5`: Roll 1d6. Runner advances only on a roll of 6.

## Runners on 1st and 2nd, less than 2 outs
 - `G1`, `G2`, `G3`, `G4`, `G6`: Play as "runner on first" and try for a double play. Runner on second advances to third.
 - `G5`: Roll 1d6 to determine where the third baseman fields the ball.
     - 1-2: The ball is hit towards the third base foul line. Try for the 53 double play as you would in the “runner on first” scenario. Note that there is no pivot in this instance and the `BR` rating of the runner on second is used. The runner on first advances to second.
     - 3-4: The ball is hit right at the third baseman. There are two choices. Choose, then roll 1d6.
          - Go for only the force play at third. On rolls of 1-5, the runner from second is forced at third. On a roll of 6, the only play is at first.
          - Try for the 543 double play and allow the runner at second to advance to third. To try for the double play, proceed as you would in the “runner on first” scenario with the second baseman’s pivot included.
     - 5-6: The ball is hit deep in the hole between third and short. No play at third. Try for the 543 double play as you would in the “runner on first” scenario. The runner on second advances to third.

## Runner on 3rd, less than 2 outs
Decide if the runner on third heads for home. If a play is to be made on the runner, roll 1d6 against his `BR` rating ([adjusted as necessary](#adjustments-for-groundouts-non-bunts)). If the roll is less than or equal to the rating, the runner scores, otherwise he is out at the plate.

Note that with the infield IN, the only plays for an infielder are home or first with the only double play chance of home to first (catcher's have no pivot rating to include). The pitcher is always considered to be IN but can make the play at any base.

## Runners on 1st and 3rd, less than 2 outs
Since there is no force play at the plate, *the first task is to decide whether the runner on third will try to score*. This decision should be heavily influenced by the infielder's depth. [2601]

>The pitcher and catcher get all the advantages of playing in but have all the choices of playing halfway and back. They can go for the double play at any time and have the `BR`-2 advantage when attempting a play on a runner going home.

### Infielder Back
It's likely that the runner on third will break for the plate. The assumption is that the defense is playing for a double play and conceding the run. If so, follow the [standard GDP procedure](#runner-on-1st-less-than-2-outs) and the runner from third scores.

If instead the defense chooses to make a play on the runner going home, that runner's `BR` rating gets a +2. For this reason, it's not a good decision to go home with the throw when playing back. Use the [runner on third procedure](#runner-on-3rd-less-than-2-outs).

### Infielder Halfway
In this case, the runner's decision is less sure. If he holds, the defense can try for the double play and let the run score. Alternatively, if he holds, the defense can decide to "look the runner back" and throw to first. This allows the runner on first to advance to second.

If the runner from third goes, the defense can decide to make a play on him (`BR` - 1) or let him score and go for the double play (second to first).

### Infielder In
If the infield is playing in, they are doing so to prevent the runner on third from scoring. If the runner decides to hold, the defense's only play is at first. If he goes, the defense can try for the play at the plate with the advantage of a `BR` -2 adjustment on the runner or simply take the out at first. No double play is possible.\*

>\*Remember that all infielders can be positioned independently. For instance, if the corner infielders (1B/3B) are in and the middle infielders (2B/SS) are playing back or halfway, the defense has more options. Since the pivot at second base is manned by fielders who are not playing in, a second to first double play is possible. [5339]

## Runners on 2nd and 3rd, less than 2 outs

Use [runner on third procedure](#runner-on-3rd-less-than-2-outs) with the runner on second moving to third.

## Bases Loaded, less than 2 outs

If the infielder is playing in, the only possible double play chance is home to first. The catcher's pivot is always zero. This is also the only double play that is possible if all infielders are in.

If the infielder is playing halfway or back, more types of double plays are possible if the pivot fielder is also not playing in.

## Further Discussion

Of the eight base states, five (`---`, `1--`, `-2-`, `--3`, `12-`) are specifically addressed by the rules. That leaves first and third (`1-3`), second and third (`-23`), and  bases loaded (`123`) situations to consider. Handle these situations using combinations of the rules. For example, first and third could proceed with a double play chance ("runner on first" scenario) or, likely in a late inning situation, a play at the plate ("runner on third" scenario").

"In those situations it is going to be based on how the defense sets up. If playing back remove the runner at 3rd in the equation which only leaves normal DPs or plays at first. If playing in DPs other than home to 1st are out and you would play it like the lead runner is on 3rd." [5323]

## Suggested Infield Depth

Each position can be set at different depths, but listed below are suggestions for when to play certain depths based on game situation.

: Suggested Infield Depth (less than 2 outs)

| `1--` | `2--` | `3--`  | `12-` | `1-3`  | `-23`  | `123`  |
|-------|-------|--------|-------|--------|--------|--------|
| Half  | Back  | Back\* | Half  | Half\* | Back\* | Back\* |

>\* 1 run game or tied, with a runner on third, all positions are In

>2 outs or 4+ run lead, any base state, all positions are Back

## Adjustments for Groundouts (Non-Bunts)

With the bases empty, an infield's normal positioning is "back". This optimizes each infielder's range, but makes it harder to make a play at the plate. The range rating listed on each infielder's card is in this "back" position.

With runners on base, a manager may want to optimize the chance of a double play. In this case, infielders will be positioned halfway ("double play depth"), a bit closer to the plate than "back", with middle infielders cheating a bit towards second base.

Bringing the infield "in" is a common tactic to prevent a runner on third from scoring. Playing closer to the hitter allows infielders to more quickly get the ball to the plate with the caveat of reduced range.

>Pitchers and catchers cannot change positioning, thus their range is the same in all situations. All other infielders may be positioned at any of the three depths.

Apply the following adjustments depending on the fielding player's positioning in non-bunting situations:

|          | Back | Half |    In    |  P  |  C  |
|:---------|:----:|:----:|:--------:|:---:|:---:|
| Range    |      |  -1  |   -2     |     |     |
| `GDP`    |      |  +1  |          | +1  |     |
| `BR` 1st |      |  -1  |          | -1  |     |
| `BR` 3rd |  +2  |  -1  |   -2     | -2  | -2  |
| Plays to | Any  |  Any | Home/1st | Any | Any |

- Infielder back: `BR + 2` (runner on third)

- Infielder halfway: `Range - 1`, `GDP + 1`, `BR - 1` (runner on third or runner on first for double play chance)

- Infielder in: `Range - 2`, `BR - 2` (runner on third), *must throw the ball to home or first*

- Pitcher (`G1`): `GDP + 1`, `BR - 2` (runner on third), `BR - 1` (runner on first) 

- Catcher (`G2`): `BR - 2` (runner on third)

>The first baseman is NOT considered to be in if holding a runner on at first. [5339]

## Adjustments for Bunt Plays

|             | Back | Half |    In    | P or C |
|:------------|:----:|:----:|:--------:|:------:|
| Range       |      |  +1  |    +2    |        |
| `GDP`       |  -1  |      |    +1    |   +1   |
| `BR` Batter |  +2  |      |    -2    |   -2   |

- Infielder back: `GDP - 1`, `BR + 2` (hitter)

- Infielder halfway: `Range + 1`

- Infielder in: `Range + 2`, `GDP + 1`, `BR - 2` (hitter)

- Pitcher (`G1`), or catcher (`G2`): `GDP + 1`, `BR - 2` (hitter)

# Lineouts

To check for a double play, roll 1d6. If there is a runner on the base of the range rolled, the runner is doubled off. Infield depth has no impact on line drives—either on the range of the fielder or by restricting the bases to which he may throw. [4902]

: Lineout Double Play Chart

|Base |`L1` |`L3` |`L4` |`L5` | `L6` |
|:---:|:---:|:---:|:---:|:---:|:----:|
| 1st | 1-4 | 1-4 | 1-2 |  1  |  1   |
| 2nd |  5  |  5  | 3-4 |  2  |  2-3 |
| 3rd |  6  |     |     |  3  |      |

>Example: Runners on 1st and 2nd, result is an `L4`. A 1d6 roll of 1 or 2 doubles off the runner at first. A roll of 3 or 4 doubles off the runner at second. Rolls of 5 or 6 have no effect.

# Flyouts

## No runners on
Simple flyout.

## Runner on 2nd, less than 2 outs
Roll 2d6 and compare the primary (red) die against the following:

- `F7` No advance (no roll necessary)
- `F8` `BR + OF Arm - 3`
- `F9` `BR + OF Arm`

If the roll of the primary die is less than or equal to the above, the runner advances from second to third. If double sixes are rolled, the runner is thrown out trying to advance. On all other rolls, the runner holds.

## Possible sacrifice fly: runner on 3rd, less than 2 outs

Roll 1d6. If the roll is less than or equal to the batter's `SF` rating, the runner on third scores without a throw to the plate. If the runner on third scores on that roll, any trailing runners trying to tag up and advance are subject to the typical ["Singles and Doubles to the Outfield" procedure](#singles-and-doubles-to-the-outfield) with `Adjusted BR` = `BR + OF Arm` procedure, requiring a 2d6 roll.

If the roll exceeds the `SF` rating, however, a throw home occurs. In that case, handle all runner andvancement by rolling 2d6 and using the ["Singles and Doubles to the Outfield" procedure](#singles-and-doubles-to-the-outfield) with `Adjusted BR` = `BR + OF Arm`. [305, 5194\*]

>Example: Runners on 2nd and 3rd with 1 out. `F9` means the batter is out and there is a possible sacrifice fly. Batter's `SF` = 2. 1d6 rolls of 1-2 and the run scores without a throw home. The rightfielder's `Arm` = +1 and the runner on second's `BR` = 3 for an `Adjusted BR` = 4. Consulting the throw chart, a throw to third is possible and a 2d6 roll is necessary to determine advancement. If the inital 1d6 roll is 3-6, the throw goes home and more possiblities ensue: The runner on 3rd's `BR` = 1 for an `Adjusted BR` = 2. Consulting the throw chart, a throw is possible and a 2d6 roll is necessary. One possibility, a roll of 6-1 on the throw chart, would mean that the runner on 3rd scores, the ball is cutoff, and the runner on 2nd is thrown out trying to advance.

>The sacrifice fly attempt is first dependent on the depth of the ball hit by the batter (`SF` rating). Think of fly balls that fail that check as average depth fly balls where the runner determines if he can beat the outfielder's throw--that's when the `Arm` and `BR` are used.


# Strategy Roll

The strategy roll is a simple engine to drive managerial decisions in an automated fashion. When runners are on base, roll the 1d20 once per plate appearance. These rolls can be played either as suggestions or hard and fast rules and should always be applied with the game situation in mind.

The strategy roll is not used when the score differential plus the inning number is greater than 10.

>Example: Score is 5-2 in the 8th … 5 - 2 + 8 = 11. No strategy rolls.

The 20 sides of the die are divided into ranges that trigger [steal attempts](#stealing), [hit and runs](#hit-and-run), [sacrifice bunts](#sacrifice-bunts), and [pickoff attempts](#pickoffs). The ranges are calculated based on the runner's `ATT` rating, the pitcher's `HLD` rating, and the batter's `BNT` rating.

: Example Strategy Roll Ranges, 0 or 1 out, Runner on 1st

|                        |       |       |      |       |
|:-----------------------|:-----:|:-----:|:----:|:-----:|
| `ATT`                  | 7     | 2     | `H`  | 13    |
| `HLD`                  | 0     | +4    | +1   | +1    |
| `BNT`                  | 3     | 4     | 2    | 5     |
| Steal attempt of 2nd   | 1-7   | 1-4*  |      | 1-14  |
| Hit and run attempt    | 8-9   | 5-6   | 1-2  | 15-16 |
| No strategy            | 10-17 | 7-16  | 3-18 |       |
| Sacrifice bunt attempt | 18-20 | 17-19 | 19   | 17-19 |
| Pickoff attempt        | 20    | 20    | 20   | 20    |

>*The `HLD` rating cannot more than double the `ATT` rating.

>On the rare instance of an overlap between stealing/hit and run and bunt attempts, give priority to the stealing/hit and run range. [4889]

>Ignore sacrifice bunt attempts with two outs. [4874]

>One very popular house rule for strategy rolls is to roll each time the on-base situation changes. After a steal or a wild pitch or passed ball, many people choose to roll again during the same plate appearance. [4437]

## Stealing

### Getting the Jump

Steal attempts are triggered by rolling 1d20 against a combination of the runner's `ATT` rating and the pitcher's `HLD` rating. Rolls less than or equal to this `Adjusted ATT` mean that the runner has "gotten the jump" and will attempt to steal.

: `ATT` Adjustment Table

| Base to Steal |  `Adjusted ATT`   |
|:-------------:|:-----------------:|
|     2nd       | `ATT + HLD`       |
|     3rd       | `(ATT + HLD) / 2` |
|     Home      | `(ATT + HLD) / 5` |

>`HLD` is the rating to the left of the `/` on a pitcher's card `SB` cell.

>A batter's `Adjusted ATT` rating cannot be more than double his `ATT` rating. A runner with an `ATT` = 2 will only see an `Adjusted ATT` rate of 4, even if the pitcher's `HLD` is +4. [4635]

Special `ATT` ratings include `N` and `H`. `N` means that the runner neither attempts to steal nor is involved in hit and runs. `H` means that the runner is only involved in hit and runs and does not attempt to steal (`ATT` = 0).

With multiple runners on base, there are no additional rolls required. A single strategy roll per plate appearance is used to check each runner's individual possibility of a stolen base attempt. With runners on first and second, the lead runner might be the only one who steals or it could be a double steal. It just depends on each of their `Adjusted ATT` ratings.

>Example: The pitcher's `HLD` rating is zero. Runner on first with an `ATT` of 1, runner on second with an `ATT` of 6. A roll of 1 means a double steal, while a 2-3 (`ATT` of 6 is halved) would mean only the runner on second is stealing. [1210]

### Attempting to Steal

If a runner gets the jump and attempts to steal, roll 1d20 against his `Adjusted SB` rating. This is the sum of his raw `SB` rating, the pitcher's `SB` rating, and the catcher's arm rating.

: `SB` Adjustment Table

| Base to Steal |                `Adjusted SB`                     |
|:-------------:|:------------------------------------------------:|
|  2nd or 3rd   | `SB` (runner) + `SB` (pitcher) + `Arm` (catcher) |
|     Home      | `SB` (runner) + `SB` (pitcher) - 10              |

>The pitcher's `SB` rating is to the right of the `/` on a pitcher's card `SB` cell.

On rolls less than or equal to this adjusted rating, the runner succesfully steals the base. A roll greater than the `Adjusted SB` rating *or* equal to 20 (regardless of `SB` rating) means that the runner is caught stealing.

On rolls of `1` and `2`, a check is made for a throwing error by the catcher. Roll another 1d20 against the catcher's error rating. If the roll is less than or equal to the rating, an error occurs and it is an automatic one base advance for all runners. [4874]

When deciding who gets thrown out on a double steal, if both runners roll a caught stealing, default to the runner stealing 3rd. [1210]

## Hit and Run

With a runner on 1st, strategy rolls 1-2 greater than the `Adjusted ATT` rating trigger a hit and run attempt.

The at bat proceeds normally with the following adjustments:

- `GDP – 2`
- `BR + 2`
- `HR / 2` and `W / 2` (round down)
- All lineouts are double plays, with the exception of an error on `ET?`.
- On a strikeout, resolve the play as a steal attempt. This is a chance for a "strike 'em out, throw 'em out" double play.

>Runners with `H` ratings always have an `Adjusted ATT` of zero, regardless of a pitcher's `HLD` rating. This means that they hit and run on rolls of 1 and 2. [305]

## Sacrifice Bunts

With a runner on first and less than two outs, strategy rolls greater than 10 are potential sacrifice bunts. The bunting chart determines the rolls for which a bunt is attempted. These ranges are based on the `BNT` rating of the batter and differ for position players and pitchers.

: Bunting Chart

|   `BNT`    | Position Players | Pitchers |
|:----------:|:----------------:|:--------:|
|     5      |       16-19      |  10-19   |
|     4      |       17-19      |  12-19   |
|     3      |       18-19      |  14-19   |
|     2      |        19        |  18-19   |
|     1      |        No        |  18-19   |

On an attempted sacrifice bunt, roll 2d6 to resolve the pitcher's card and disregard all results other than the following:

- `HBP` and `WLD`: resolve normally
- `K` and `K+`: cut the batter's `K` rating in half (round down)
- `RP` and `RP@`: failed range play results in a single (all advance one base), successful range play results in a sacrifice bunt
- `E?`, `ET?`, and `EG?`: proceed with the sacrifice attempt, then check for a fielder error

Roll 1d6 to get the player fielding the ball:

| 1d6 | Bunt Fielder |
|:---:|:------------:|
| 1-2 |      P       |
| 3-4 |      1B      |
|  5  |      3B      |
|  6  |      C       |

Roll 1d20 to get the result of the bunt attempt.

: Bunt Chart

| Result               |   0   |   1   |   2   |   3   |   4   |   5   |
|:---------------------|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
| Successful Bunt      | 1-6   | 1-8   | 1-10  | 1-12  | 1-14  | 1-16  |
| Foul                 | 7-9   | 9-10  | 11-12 | 13    | 15    |       |
| Lead Runner Out      | 10-13 | 11-14 | 13-15 | 14-16 | 16-17 | 17-18 |
| Safe (Single)        |       |       | 16    | 17    | 18-19 | 19-20 |
| Bunted Too Hard, GDP | 14-18 | 15-18 | 17-19 | 18-19 | 20    |       |
| Popout, DP?          | 19-20 | 19-20 | 20    | 20    |       |       |

>Note: Subtract 1 from `BNT` rating if the player fielding the ball is playing IN. Neither the pitcher nor the catcher fielding the ball causes the `BNT` rating to change. [5976] Typical positioning for a likely bunt would be to play the "corners in", and any balls fielded by the first or third basemen would reduce the `BNT` rating.

The results from the bunt chart:

A successful sacrifice bunt means that the batter is out at first and all other runners advance one base.

On a foul result, either redo the sacrifice attempt or swing away with a regular at bat.

If the lead runner is thrown out, the batter is safe at 1st on a fielder's choice.

A safe result means that everyone is safe with a single.

When bunted too hard, resolve the play as a normal ground ball double play chance.

Popout DP? The batter pops out. Roll 1d6 against the lead runner's `BR` rating. If roll is greater than his `BR`, the runner is out.

## Pickoffs

A strategy roll of 20 leads to a pickoff attempt. Roll an additional 1d20 to compare with the pitcher's `BLK` (balk), `PO` (pickoff), and `POE` (pickoff error) ratings. Rolls in these ranges trigger the corresponding results. A `POE` is when the pitcher throws the ball away, resulting in all runners advancing one base. This is automatic, without regard for the pitcher's error rating.

>With multiple runners on base, apply the pickoff to a runner with a base open in front of them. For example, the runner on first in a first and third situation and the runner on second in a first and second situation. Otherwise, go for the runner on third. [4526]

## Bunting for a hit

When bunting for a hit, follow the sacrifice bunt procedure. If the result from the bunt chart is a successful bunt, roll another 1d20. If the roll is less than or equal to (batter's `BNT` + batter's `BR` - fielder's range rating), it's a single. Otherwise it's a groundout.

## Squeeze Bunts

Follow the [sacrifice bunt procedure](#sacrifice-bunts), but use the squeeze bunt chart below. If batter strikes out, the runner from third attempts a steal of home using `SB - 10`.

: Squeeze Bunt Chart

| Result          |   0   |   1   |   2   |   3   |   4   |   5   |
|:----------------|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
| Successful Bunt | 1     | 1-2   | 1-4   | 1-6   | 1-8   | 1-10  |
| Foul            | 2-5   | 3-6   | 5-7   | 7-8   | 9     |       |
| Lead Runner Out | 6-11  | 7-11  | 8-11  | 9-11  | 10-11 | 11    |
| ?? Bunt         | 12-16 | 12-16 | 12-17 | 12-17 | 12-18 | 12-18 |
| Rundown         | 17    | 17    | 18    | 18    | 19    | 19    |
| Popout, DP      | 18-20 | 18-20 | 19-20 | 19-20 | 20    | 20    |

>Note: Subtract 1 from `BNT` rating if infield in.

A successful bunt means the only play is on the batter going to first.

On a foul result, either redo the squeeze attempt or swing away with a regular at bat.

If the lead runner is thrown out, the batter is safe at 1st on a fielder's choice.

?? Bunt: The defense has a choice, (1) retire batter at first or (2) attempt play at the plate. If attempting the play at the plate, roll 1d6. A roll less than or equal to the `BR` rating of the runner from third and he scores, otherwise he is thrown out at the plate. In this situation, if the infielder is playing in, decrease the `BR` rating of the runner from third by 1.

Rundown: The bunt is too hard and the runner from third is caught up in a rundown. Proceed to roll 1d6 and use the rundown chart.

Popout DP: The batter and runner from third are both out.

# Pitching Changes

## Fatigue
A pitcher is considered tired once they have reached the number of batters faced shown on their cards next to `START` or `RELIEF`. When a pitcher is tired, the `(S1)` and `K(W)` results become the result in parentheses. See [2.3](#s1-1) and [2.14](#kw), respectively.

>The cards are designed with the idea that a pitcher will face approximately 5-10% of batters while tired. [4354]

## Pull (Optional)
The number to the right of the `/` on a pitcher's `START` or `RELIEF` lines is the pull rating. Once the total number of runs, hits, and unintentional walks exceeds the pull rating, the pitcher should be removed from the game.

>This is a simplified guideline to give an idea of when the pitcher normally leaves a game. [5148]

## Exhausted (Optional)
When a tired pitcher gets a `(??)`, `(S1)`, or `K(W)` result, the pitcher becomes exhausted and must be replaced before facing another batter.

# Injuries
When an injury check is triggered by a rare play result, roll 1d6 against the Injury Chart below to determine the time (if any) the player misses.

: Injury Chart

|1d6|`INJ-0`          |`INJ-1`          |`INJ-2`          |`INJ-3`          |`INJ-4`          |`INJ-5`          |`INJ-6`          |`INJ-7`    |
|:-:|:---------------:|:---------------:|:---------------:|:---------------:|:---------------:|:---------------:|:---------------:|:---------:|
| 1 |None             |None             |None             |Game             |Game             |Game + 1d6       |Game + 2d6       |Game + 1d20|
| 2 |None             |None             |Game             |Game             |Game + 1d6       |Game + 2d6       |Game + 1d20      | 15 day    |
| 3 |None             |Game             |Game             |Game + 1d6       |Game + 2d6       |Game + 1d20      |15 day           |15 day     |
| 4 |Game             |Game             |Game + 1d6       |Game + 2d6       |Game + 1d20      |15 day           |15 day           |60 day     |
| 5 |Game             |Game + 1d6       |Game + 2d6       |Game + 1d20      |15 day           |15 day           |60 day           |60 day     |
| 6 |Reroll on `INJ-1`|Reroll on `INJ-2`|Reroll on `INJ-3`|Reroll on `INJ-4`|Reroll on `INJ-5`|Reroll on `INJ-6`|Reroll on `INJ-7`|60 day     |

1d6 number of days

sum of 2d6 number of days

1d20 number of days

<footer><p>Changelog: [https://gitlab.com/brewerja/inside-pitch-rules/-/commits/master](https://gitlab.com/brewerja/inside-pitch-rules/-/commits/master)</p></footer>
